CC := gcc
LD := gcc
CFLAGS := -g -Wall -Werror -O0 
LDFLAGS := 
LIBS := -lc

APP_NAME := 16bit_cpu_emulator

SRC := ./src/
BUILD := ./build/
C_SRC := $(wildcard $(SRC)/*.c)
C_OBJ := $(C_SRC:.c=.o)

.PHONY: clean run

$(BUILD)/$(APP_NAME): $(C_OBJ)
	$(LD) $(LDFLAGS) $^ -o $@ $(LIBS)

run: $(BUILD)/$(APP_NAME)
	$(BUILD)/$(APP_NAME)

%.o: %.c 
	$(CC) $(CFLAGS) -c $^ -o $@

clean: 
	-rm -rf $(BUILD)/*
	-find $(SRC)/ -name *.o -delete
