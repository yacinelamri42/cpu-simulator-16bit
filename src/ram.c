#include "ram.h"
#include <stdlib.h>

struct RAM new_ram(size_t ram_size) {
    struct RAM ram;
    ram.ram_size = ram_size;
    ram.ram = (uint8_t*)calloc(ram.ram_size, 1);
    return ram;
}

void free_ram(struct RAM ram) {
    free(ram.ram);
}
