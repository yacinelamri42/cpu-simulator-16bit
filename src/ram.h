#ifndef RAM__H
#define RAM__H

#include <stddef.h>
#include <stdint.h>

struct RAM {
    uint8_t * ram;
    size_t ram_size;
};

struct RAM new_ram(size_t ram_size);
void free_ram(struct RAM ram);

#endif // !RAM__H
