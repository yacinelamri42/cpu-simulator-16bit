#include <stddef.h>
#include "cpu.h"
#include "ram.h"

struct CPU new_cpu(size_t ram_size) {
    struct CPU cpu;
    
    cpu.registers.ra.raw = 0;
    cpu.registers.rb.rbw = 0;
    cpu.registers.rc.rcw = 0;
    cpu.registers.rd.rdw = 0;
    
    cpu.registers.bp = 0;
    cpu.registers.sp = 0;

    cpu.registers.ip = 0;
    
    cpu.registers.eflags = 0;
    
    cpu.registers.cs = 0;
    cpu.registers.ds = 0;
    cpu.registers.ss = 0;
    cpu.registers.es = 0;
    cpu.registers.fs = 0;
    cpu.registers.gs = 0;

    cpu.memory = new_ram(ram_size);

    return cpu;
}

void run(struct CPU cpu) {

}

void free_cpu(struct CPU cpu) {
    free_ram(cpu.memory);
}
