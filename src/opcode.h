#ifndef OPCODE__H
#define OPCODE__H

// instruction format<16bits>: opcode<8bits> other_num<8bits>


// general register numbers:
// if first bit = 0, i m pointing to a byte register
// if first bit = 1, i m pointing to a 16bit register
// 0 00 1	=> pointing to registers.ra.rab[1]
// 1 0 00 	=> pointing to 16bit register registers.ra.raw (bit in the middle is always 0)
// 1 0 11	=> pointing to 16bit register registers.rd.rdw (bit in the middle is always 0)


// segment/stack register numbers:
// if first bit = 0, i m pointing to a stack register
// if first bit = 1, i m pointing to a segment register
// 0 000	=> stack register 0(bp)
// 0 001	=> stack register 0(sp)
// 1 000	=> segment register 0(cs)
// 1 001	=> segment register 0(ds)
// 1 010	=> segment register 0(es)
// 1 011	=> segment register 0(fs)
// 1 100	=> segment register 0(gs)
// 1 101	=> segment register 0(ss)

// no operation
#define OP_NOP 		0x0 	

// Move between general registers a byte
// movb register_number_destination_byte<4bits>, register_number_source_byte<4bits>
#define OP_MOVGB 	0x1 	

// Move between general registers a word
// movw register_num_destination_word<4 bits>, register_num_source_word<4bits>
#define OP_MOVGW 	0x2 

// Move word from general register to segment/stack register
// movw register_num_destination_word<4 bits>, register_num_source_word<4bits>
#define OP_MOVSG 	0x3 

// Move word from segment/stack register to general register
// movw register_num_destination_word<4 bits>, register_num_source_word<4bits>
//
#define OP_MOVSG 	0x3 

#endif // !OPCODE__H
