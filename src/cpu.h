#ifndef CPU__H
#define CPU__H

#include <stdint.h>

#include "ram.h"

struct CPU {
    struct Registers {
	union {
	    uint16_t raw;
	    uint8_t rab[2];
	} ra;
	union {
	    uint16_t rbw;
	    uint8_t rbb[2];
	} rb;
	union {
	    uint16_t rcw;
	    uint8_t rcb[2];
	} rc;
	union {
	    uint16_t rdw;
	    uint8_t rdb[2];
	} rd ;
	uint16_t sp;
	uint16_t bp;
	uint16_t ip;
	uint32_t eflags;

	uint16_t cs; // code segment
	uint16_t ds; // data segment
	uint16_t ss; // stack segment

	// Extra segments for use
	uint16_t es;
	uint16_t fs;
	uint16_t gs;
    } registers;

    struct RAM memory;
};

struct CPU new_cpu(size_t ram_size);
void run(struct CPU cpu);
void free_cpu(struct CPU cpu);

#endif // !CPU__H
